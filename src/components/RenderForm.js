import React, { useState } from "react";

import GeneralInformation from "./GeneralInformation";
import EducationExperience from "./EducationExperience";
import WorkExperience from "./WorkExperience";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";

import Divider from "@material-ui/core/Divider";

function RenderForm(props) {
  const [general, setGeneral] = useState([]);
  const [education, setEducation] = useState([]);
  const [work, setWork] = useState([]);

  function handleChange(e) {
    const value = e.target.value;
    const formName = e.target.parentNode.parentNode.parentNode.id;
    let hostObj = {};
    const newObj = {[e.target.name]: value};

    switch(formName) {
      case "general":
        hostObj = Object.assign(hostObj, general, newObj);
        setGeneral(hostObj);
        break;
      case "education":
        hostObj = Object.assign(hostObj, education, newObj);
        setEducation(hostObj);
        break;
      case "work":
        hostObj = Object.assign(hostObj, work, newObj);
        setWork(hostObj);
        break;
    }
  }

  function onSubmit(e) {
    e.preventDefault();
    const currentState = {work, general, education};
    console.log(currentState);
    props.submitExperience(e, currentState, e.target.id); // Gets the ID of the form being submitted.

    // Reset the form and state so we don't get duplicate entries.
    e.target.reset();
    //this.setState({ work: {}, general: {}, personal: {} });
  }

    return (
      <Card className="root">
        <CardContent>
          <List>
            <GeneralInformation
              onSubmit={onSubmit}
              handleChange={handleChange}
              formFields={general}
            />
          </List>
          <Divider />
          <List>
            <EducationExperience onSubmit={onSubmit} handleChange={handleChange} />
          </List>
          <Divider />
          <List>
            <WorkExperience onSubmit={onSubmit} handleChange={handleChange} />
          </List>
        </CardContent>
      </Card>
    );
}

export default RenderForm;
