import React, { useState } from "react";

import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import EditIcon from "@material-ui/icons/Edit";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

function RenderList(props) {
  const [general, setGeneral] = useState([]);
  const [education, setEducation] = useState([]);
  const [work, setWork] = useState([]);

  const { isEdit, handleEdit, formId } = props;

  function handleChange(e) {
    e.preventDefault();
    const value = e.target.value;
    const formName = e.target.parentNode.parentNode.parentNode.parentNode.id;
    let hostObj = {};
    const newObj = {[e.target.name]: value};

    switch(formName) {
      case "general":
        hostObj = Object.assign(hostObj, general, newObj);
        setGeneral(hostObj);
        break;
      case "education":
        hostObj = Object.assign(hostObj, education, newObj);
        setEducation(hostObj);
        break;
      case "work":
        hostObj = Object.assign(hostObj, work, newObj);
        setWork(hostObj);
        break;
    }
  }

  function onSubmit(e) {
    e.preventDefault();
    //const currentState = this.state;
    switch(e.target.id) {
      case "general":
        props.handleEdit(e, general, e.target.id, e.target.getAttribute("index"));
        break;
      case "work":
        props.handleEdit(e, work, e.target.id, e.target.getAttribute("index"));
        break;
      case "education":
        props.handleEdit(e, education, e.target.id, e.target.getAttribute("index"));
        break;
    }

    // Reset the form and state so we don't get duplicate entries.
    e.target.reset();
    //this.setState({ work: {}, general: {}, personal: {} });
  }

  function createList(list) {

    const content = [];
    let i = 0;

    for (const [key, value] of Object.entries(list)) {
      content.push(
        !isEdit ? (
          <div key={i}>
            <Typography variant="subtitle2" color="textSecondary">
              {key}
            </Typography>
            <Typography>{value}</Typography>
          </div>
        ) : (
          <div key={i}>
            <Typography variant="subtitle2" color="textSecondary">
              {key}
            </Typography>
            <TextField id={key} name={key} label={value} onChange={handleChange}/>
          </div>
        )
      );
      i++;
    }
    return content;
  }

  return (
    <Card key={props.index}>
      <CardContent>
        {!props.isEdit ? (
          createList(props.info)
        ) : (
          <form id={props.formId} index={props.index} onSubmit={onSubmit}>
            {createList(props.info)}
            <Button variant="contained" color="primary" type="submit">
                Done
            </Button>
          </form>
        )}
        <EditIcon id={"edit-item-" + props.index} key={props.index} onClick={props.onEditClick} />
      </CardContent>
    </Card>
  );
}

export default RenderList;
