import React, { Component } from "react";

import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

function WorkExperience(props) {
  return (
      <form id="work" onSubmit={props.onSubmit}>
        <Typography color="textSecondary" gutterBottom>
          Work Experience
        </Typography>
        <TextField id="standard-basic" label="Employer Name" name="Employer Name" onChange={props.handleChange} />
        <TextField id="standard-basic" label="Job Title" name="Job Title" onChange={props.handleChange} />
        <TextField
          id="date-from"
          label="Date from"
          name="Date From"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={props.handleChange}
        />
        <TextField
          id="date-to"
          label="Date to"
          name="Date To"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={props.handleChange}
        />

        <TextField
          id="main-tasks"
          label="Main Tasks"
          name="Main Tasks"
          type="text"
          onChange={props.handleChange}
        />
        <Button variant="contained" color="primary" type="submit">
          Done
        </Button>
      </form>
    );
}

export default WorkExperience;
