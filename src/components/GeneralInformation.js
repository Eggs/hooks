import React, { Component } from "react";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import "./GeneralInformation.css";

function GeneralInformation (props) {
  return (
      <form id="general" onSubmit={props.onSubmit}>
        <Typography color="textSecondary" gutterBottom>
          Personal Details
        </Typography>
        <TextField
          id="standard-basic"
          label="First Name"
          name="First Name"
          onChange={props.handleChange}
        />
        <TextField id="standard-basic" label="Last Name" name="Last Name" onChange={props.handleChange} />
        <TextField id="standard-email" label="Email Address" name="Email Address" onChange={props.handleChange} />
        <TextField
          id="standard-number"
          label="Telephone Number"
          name="Telephone Number"
          onChange={props.handleChange}
        />

        <Button variant="contained" color="primary" type="submit">
          Done
        </Button>
      </form>
    );
}

GeneralInformation.defaultProps = {
  itemToEdit: { "First Name": "" },
};

export default GeneralInformation;
