import React, { useState } from "react";

import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import WorkIcon from "@material-ui/icons/Work";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import GradeIcon from "@material-ui/icons/Grade";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";

import RenderList from "./components/RenderList";
import RenderForm from "./components/RenderForm";

function App() {
  const [general, setGeneral] = useState([]);
  const [education, setEducation] = useState([]);
  const [work, setWork] = useState([]);
  const [isEdit, setIsEdit] = useState(false);

  function submitExperience(e, data, section) {
    e.preventDefault();
    let updateInfo = [];
    switch (section) {
      case "work":
        updateInfo = [...work];
        updateInfo.push(data[section]);
        setWork(updateInfo);
        break;
      case "general":
        updateInfo = [...general];
        updateInfo.push(data[section]);
        setGeneral(updateInfo);
        break;
      case "education":
        updateInfo = [...education];
        updateInfo.push(data[section]);
        setEducation(updateInfo);
        break;

      default:
        break;
    }

  }

  function handleEdit(e, data, section, index) {
    e.preventDefault();
    let updateInfo = [];
    switch (section) {
      case "work":
        updateInfo = [...work];
        updateInfo[index] = Object.assign(updateInfo[index],updateInfo[index],data);
        setWork(updateInfo);
        setIsEdit(false);
        break;
      case "general":
        updateInfo = [...general];
        updateInfo[index] = Object.assign(updateInfo[index],updateInfo[index],data);
        setGeneral(updateInfo);
        setIsEdit(false);
        break;
      case "education":
        updateInfo = [...education];
        updateInfo[index] = Object.assign(updateInfo[index],updateInfo[index],data);
        setEducation(updateInfo);
        setIsEdit(false);
        break;

      default:
        break;
    }
  }

  function onEditClick(e) {
    setIsEdit(!isEdit);
  }

  return (
    <div className="App">
      <Grid container spacing={2} direction="row">
        <Grid item>
          <Grid container spacing={2} direction="column">
            <RenderForm submitExperience={submitExperience}/>
          </Grid>
        </Grid>
        <Grid item xs>
          <Grid container>
            <Grid item xs={12}>
              <Paper elevation={3}>
                <List>
                  <Typography variant="h1"> C.V.</Typography>
                  <ListItem>
                    <AccountBoxIcon />
                    <Typography variant="h6">Personal:</Typography>
                  </ListItem>
                  <ListItem>
                    {general.map((info, index) => {
                      return (
                        <RenderList
                          key={index}
                          info={info}
                          index={index}
                          handleEdit={handleEdit}
                          onEditClick={onEditClick}
                          isEdit={isEdit}
                          formId={"general"}
                          submitExperience={submitExperience}
                        />
                      );
                    })}
                  </ListItem>

                  <ListItem>
                    <GradeIcon />
                    <Typography variant="h6">Education:</Typography>
                  </ListItem>
                  <ListItem>
                    {education.map((info, index) => {
                      return (
                        <RenderList
                          key={index}
                          info={info}
                          index={index}
                          handleEdit={handleEdit}
                          onEditClick={onEditClick}
                          isEdit={isEdit}
                          formId={"education"}
                          submitExperience={submitExperience}
                        />
                      );
                    })}
                  </ListItem>

                  <ListItem>
                    <WorkIcon />
                    <Typography variant="h6"> Work:</Typography>
                  </ListItem>

                  <ListItem>
                    {work.map((info, index) => {
                      return (
                        <RenderList
                          key={index}
                          info={info}
                          index={index}
                          handleEdit={handleEdit}
                          onEditClick={onEditClick}
                          isEdit={isEdit}
                          formId={"work"}
                          submitExperience={submitExperience}
                        />
                      );
                    })}
                  </ListItem>
                </List>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}




export default App;
